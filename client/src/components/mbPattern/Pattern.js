import React from 'react';
import {connect} from 'react-redux';
class Component extends React.Component{//{{{
  //events{{{
  test=()=>{
    return this.state.myArray.map(item=>{
      return (
        <p key={item._id}>{item.name}
        </p>
      );
    })
  }
  //events}}}
  
  render(){//{{{
    const {posts}=this.props;
    return (
      <div className='pattern'>
       {JSON.stringify(posts)}
      </div>
    );
  }//}}}
}//}}}
  const mapStateToProps=(state)=>{//{{{
    return {
      posts:state.posts
    }
  }//}}}
export default connect(mapStateToProps)(Component)
