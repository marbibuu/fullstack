import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';

import UserCard from './Card';
import {Container,Grid} from '@material-ui/core';

import {userCtrl} from '../../logic/core';

const styles=theme=>({
  container:{
    "margin-top":40
  },
});
class Component extends React.Component{//{{{
  displayUserCards=(classes)=>{//{{{
    const {users}=this.props;
    return users.map(user=>{
      return (
        <UserCard
          key={user._id}
          user={user}
        />
      )
    })
  }//}}}
  render(){//{{{
    const {classes}=this.props;
    return (
      <React.Fragment>
      <Container
      maxWidth='lg'
      className={classes.container}
      >
        <Grid container
          spacing={5}
          direction='row'
          justify='center'
          alignItems='center'
        >{this.displayUserCards(classes)}
        </Grid>
      </Container>
      </React.Fragment>
    );
  }//}}}
}//}}}
const mapStateToProps=(state)=>{//{{{
  return {
    users:userCtrl.getAll(state)
  }
}//}}}
export default connect(mapStateToProps)(withStyles(styles)(Component));
