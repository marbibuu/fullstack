import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {AppBar,Toolbar,Button,IconButton,Typography,Grid,FormControl,TextField} from '@material-ui/core';
import {ArrowBack,AddCircle} from '@material-ui/icons';


import MbDialog from '../utils/Dialog';
import CreatePostForm from '../post/forms/CreatePost';

import {userCtrl,postCtrl} from '../../logic/core';

//styles{{{
const styles=theme=>({
  toolbar:{
    background:'transparent',
    verticalAlign:'center',
  },
  title:{
    textTransform:'capitalize',
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'18',
  },
  backBtn:{
    textTransform:'capitalize !important',
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(){//{{{
    super();
    this.state={
      dialog_addPost_visible:false,
    }
  }//}}}
  setAddPostDialogVisibility=value=>{//{{{
    this.setState({
      dialog_addPost_visible:value
    })
  }//}}}
  handleClick_showAddPostDialog=async e=>{//{{{
    this.setAddPostDialogVisibility(true);
  }//}}}
  handleClick_hideAddPostDialog=e=>{//{{{
    this.setAddPostDialogVisibility(false);
  }//}}}
  handleClick_addPost=async (e,data)=>{//{{{
    await postCtrl.create({
      userID:this.props.user._id,
      ...data
    });
    this.setAddPostDialogVisibility(false)
  }//}}}
  handleClick_back=e=>{//{{{
    let items=this.props.location.pathname.split('/');
    if(items.length===3){
      this.props.history.push(`/`);
    }else{
      this.props.history.push(items.slice(0,-1).join('/'));
    }
  }//}}}
  render(){//{{{
    const {classes,user,location,history}=this.props;
    const dialog=<MbDialog
        dialog={{
          timeout:3000,
          visible:this.state.dialog_addPost_visible,
          title:"add post",
        }}
        onClose={this.handleClick_hideAddPostDialog}
        onAccept={this.handleClick_addPost}
      >
        <CreatePostForm></CreatePostForm>
      </MbDialog>
    var addPostButton;
    if(location.pathname.split('/').length===3){
      addPostButton=<IconButton
      size='small'
      onClick={this.handleClick_showAddPostDialog}
          >
            <AddCircle
              color='primary'
              fontSize='large'
            />
          </IconButton>
    }
    return (
      <React.Fragment>
        <AppBar
          position='static'
          elevation={0}
          className={classes.toolbar}
        >
      <Toolbar>
      <Grid container
        direction='row'
        justify='space-between'
        alignItems='center'
      >
        <Grid item>
          <Button
            onClick={this.handleClick_back}
          >
            <ArrowBack
              color='primary'
              fontSize='large'
            />
            <Typography
              color='primary'
              className={classes.backBtn}
              >back
            </Typography>
          </Button>
        </Grid>
        <Grid item>
          <Typography
            className={classes.title}
            >{user.name} {user.surname}
          </Typography>
        </Grid>
        <Grid item>

      {addPostButton} 

        </Grid>
      </Grid>
      </Toolbar>
      </AppBar>


      {dialog} 
      </React.Fragment>
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return {
    user:userCtrl.getByID(state,props.match.params.id),
  }
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
