import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Grid,Card,CardContent,CardActions,Button,Typography} from '@material-ui/core';

const styles=theme=>({//{{{
  card:{
    width:200,
    cursor:'pointer',
    border:'2px solid #000',
    borderRadius:0,
  },
  userName:{
    textTransform:'capitalize',
    textAlign:'left',
    textWeight:'bold',
  },
  detailsButton:{
    background:'transparent',
    textTransform:'capitalize!important'
  },
  verticalSpacer:{
    flexDirection:'column',
    flexGrow:1
  },
  text:{
    fontSize:15,
    color:'blue',
    textDecoration:'underline'
  }
});//}}}

class Component extends React.Component{//{{{
  //events{{{
  test=()=>{
    return this.state.myArray.map(item=>{
      return (
        <p key={item._id}>{item.name}
        </p>
      );
    })
  }
  //events}}}
  render(){//{{{
    const {classes,user,history}=this.props;
    return (
      <Grid item
        key={user._id}
        >
        <Card
          variant='outlined'
          className={classes.card}
          p={10}
      style={{display:'flex',flexDirection:'column'}}
        >
          <CardContent
            justify='left'
          >
      <Typography
      variant='h6'
        className={classes.userName}
      >{user.name} {user.surname}
      </Typography>
      <Typography
      className={classes.text}
        style={{fontStyle:'italic'}}
      >{user.email}
      </Typography>
      <Typography
      className={classes.text}
      >{user.mobile}
      </Typography>
      <Typography
        className={classes.verticalSpacer,classes.text}
      >{user.website}
      </Typography>

          </CardContent>
      <CardActions
      style={{alignSelf:'flex-center',margin:'0px 20px 10px'}}
      >
      
         <Button
      className={classes.detailsButton}
      variant="contained"
           onClick={e=>{history.push(`/user/${user._id}`)}}
           fullWidth={true}
           elevation={5}
         >Details
      </Button>
      </CardActions>
        </Card>
      </Grid>
    );
  }//}}}
}//}}}
  const mapStateToProps=(state,props)=>{//{{{
    return state;
  }//}}}

export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
