import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Grid,TextField,TextArea,Typography,Button} from '@material-ui/core';
import {Alert} from '@material-ui/lab';

//styles{{{
const styles=theme=>({
  label:{
    marginLeft:10,
    marginRight:10,
    width:50,
    textTransform:'capitalize'
  },
  input:{
    border:'2px solid #000',
    marginBottom:10,
    flex:1,
  },
  alert:{
    marginBottom:10,
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
    }
  }//}}}
  render(){//{{{
    const {classes,rows,multiline,value,label,name,handleChange,error,history}=this.props;

    let alert;
    if(error){
      alert=<Alert
        className={classes.alert}
        severity='error'
        >{error}</Alert>
    }
    return (
      <React.Fragment>
        <Grid container
          direction='row'
        >
          <Typography
            className={classes.label}
            >{label}
          </Typography>
          <TextField
            value={value}
            rows={rows}
            name={name?name:label}
            onChange={handleChange}
            className={classes.input}
            variant='outlined'
            error={error!==""}
            spellCheck={false}
            multiline={multiline}
          ></TextField>

        </Grid>
        {alert}
      </React.Fragment>
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return state;
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
