import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Dialog,DialogTitle,Card,AppBar,Toolbar,Button,Divider,
  Typography,CircularProgress} from '@material-ui/core'
import {DeleteOutlined,KeyboardArrowRight} from '@material-ui/icons';

import mb from '../../logic/mb';


//styles{{{
const styles=theme=>({
  modal:{
    border:'1px solid #000'
  },
  dialog:{
    width:400,
    borderRadius:0,
  },
  title:{
    textAlign:'center',
    fontWeight:'bold',
    textTransform:'capitalize'
  },
  spacer:{
    flexGrow:1
  },
  toolbar:{
    background:'transparent'
  },
  loader:{
    marginRight:5,
    color:'white'
  },
  form:{
    marginLeft:20,
    marginRight:20
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
      loader:false,
      data:{},
    };
  }//}}}

setLoaderVisibility=value=>{//{{{
  this.setState({
    loader:value
  })
}//}}}
showLoader=()=>{//{{{
  this.setLoaderVisibility(true);
}//}}}
hideLoader=()=>{//{{{
  this.setLoaderVisibility(false);
}//}}}
handleClick_closeDialog=e=>{//{{{
  this.props.onClose(e);
}//}}}
handleClick_acceptDialog=async e=>{//{{{
  this.showLoader();
  await mb.sleep(this.props.dialog.timeout,e,this.props.onAccept,this.state.data);
  this.hideLoader();
}//}}}
handleChange=async data=>{//{{{
  let newState={...this.state}
  newState.data=data;
  await this.setState(newState)
}//}}}
  render(){//{{{
    const {classes,dialog,history}=this.props;
    this.form=React.cloneElement(this.props.children,{
      onChange:this.handleChange
    })
    let loaderComponent;
    if(this.state.loader){
      loaderComponent=<CircularProgress
        size={20}
        disableShrink={true}
        className={classes.loader}
        variant='indeterminate'
        thickness={2}>
      </CircularProgress>
    }
    return (
      <Dialog
      className={classes.modal}
        open={dialog.visible}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
      <Typography>
      {dialog.title}
      </Typography>
      <Divider
      />
        <DialogTitle
          className={classes.title}
        >{dialog.title}
        </DialogTitle>
      <Card
        className={classes.dialog}
      elevation={0}
      >
<Card
      className={classes.form}
      elevation={0}
      >
     <div ref='wrapper'> 
      {this.form}
      </div>
      </Card>
        <AppBar
          className={classes.toolbar}
          position='static'
        >
      <Toolbar
          variant='dense'
      >
      <Typography
      className={classes.spacer}
      >
      </Typography>
        <Button
      onClick={this.handleClick_closeDialog}
      variant='contained'
      mr={2}
      >cancel</Button>
        <Button
          onClick={this.handleClick_acceptDialog}
          variant='contained'
          color='primary'
          disabled={this.state.data==={}}
        >
        {loaderComponent}
      save
      </Button>
      </Toolbar>
        </AppBar>


        </Card>
        <div
      height={20}
      >
        </div>
      </Dialog>
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return state//{
    /*dialog:{
      visible:state.app.dialog.visible,
      title:'Add post'
    }*/
  //}
}//}}}
const mapDispatchToProps=(dispatch)=>{//{{{
  return {
    changeDialogVisibility:()=>{
      dispatch({
        type:'CHANGE_DIALOG_VISIBILITY',
      })
    }
  }
}//}}}
export default connect(mapStateToProps,mapDispatchToProps)(withRouter(withStyles(styles)(Component)));
