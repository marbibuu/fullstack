import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {AppBar,Toolbar,Typography,Grid,Card,Button} from '@material-ui/core';
import {commentCtrl} from '../../logic/core';

import MbDialog from '../utils/Dialog';
import UpdateCommentForm from '../comment/forms/UpdateComment';

//styles{{{
const styles=theme=>({
  toolbar:{
    background:'transparent',
    verticalAlign:'center',
  },
  title:{
    textTransform:'capitalize',
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'15',
    fontFamily:'sans-serif'
  },
  spacer:{
    flexGrow: 1,
  },
  comment:{
    border:'2px solid #000',
    borderRadius:0,
  },
  email:{
    textDecoration:'underline'
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
      dialog_updateComment_visible:false
    };
  }//}}}

  setUpdateCommentDialogVisibility=value=>{//{{{
    this.setState({
      dialog_updateComment_visible:value
    })
  }//}}}
  handleDblClick_showUpdateCommentDialog=async e=>{//{{{
    this.setUpdateCommentDialogVisibility(true);
  }//}}}
  handleClick_hideUpdateCommentDialog=e=>{//{{{
    this.setUpdateCommentDialogVisibility(false);
  }//}}}
  handleClick_updateComment=async (e,data)=>{//{{{
    await commentCtrl.update({
      commentID:this.props.comment._id,
      ...data
    });
    this.setUpdateCommentDialogVisibility(false)
  }//}}}

  render(){//{{{
    const {classes,comment,history}=this.props;
    const dialog=<MbDialog
        dialog={{
          timeout:3000,
          visible:this.state.dialog_updateComment_visible,
          title:"update comment",
        }}
        onClose={this.handleClick_hideUpdateCommentDialog}
        onAccept={this.handleClick_updateComment}
      >
        <UpdateCommentForm
          comment={comment}
        />
      </MbDialog>
    return (
      <React.Fragment>
        <Grid item>
          <Card
            className={classes.comment}
            onDoubleClick={this.handleDblClick_showUpdateCommentDialog}
            >
            <AppBar
              className={classes.toolbar}
              elevation={0}
              position='static'
            >
              <Toolbar
                variant='dense'
              >
                <Typography
                  className={classes.title}
                >{comment.name}
                </Typography>
                <Typography
                  className={classes.spacer}
                >
                </Typography>
                <Typography
                  className={classes.email}
                  color='primary'
                >
                {comment.email}
                </Typography>
              </Toolbar>
            </AppBar>
            <Typography>
            {comment.text}
            </Typography>
          </Card>
        </Grid>
      {dialog}
      </React.Fragment>
    );
  }//}}}
}//}}}

const mapStateToProps=(state,props)=>{//{{{
  return state;
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
