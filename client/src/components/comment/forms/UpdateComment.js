import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Grid} from '@material-ui/core';

import MbFormInput from '../../utils/mbInput';
import mb from '../../../logic/mb';

//styles{{{
const styles=theme=>({
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
      data:{
        name:props.comment.name,
        text:props.comment.text,
        email:props.comment.email
      },
      errors:{
        name:'',
        text:'',
        email:''
      },
    }
  }//}}}
  handleChange=e=>{//{{{
    e.preventDefault();
    const {name,value}=e.target;
    let errors=this.state.errors;
    switch(name){
      case 'name':
        errors.name=value.length<=0?'Name should have at least one character.':'';
        break;
      case 'text':
        errors.text=value.length<=0?'Body should have at least one character.':'';
        break;
      case 'email':
        errors.email=(!mb.validateEmail(value))?`It's not valid email address.`:'';
        break;
      default:
        break;
    }
    this.setState({
      data:{...this.state.data,...{[name]:value}}
    })
    try{
      this.props.onChange(this.state.data);
    }catch(err){
    }
  }//}}}
  validateForm = (errors) => {//{{{
    let valid=true;
    Object.values(errors).forEach(
      (val) => val.length > 0 && (valid = false)
    );
    return valid;
  }//}}}
  handleSubmit=e=>{//{{{
    e.preventDefault();
    if(this.validateForm(this.state.errors)) {
      console.info('Valid Form')
      console.log(this.state)
    }else{
      console.error('Invalid Form')
    }
    return this.state.data;
  }//}}}
  render(){//{{{
    const {classes,user,history}=this.props;
    return (
      <form
        action='/'
        onSubmit={this.handleSubmit}
        autoComplete="off"
        > 
        <Grid container
          direction='column'
        >
          <MbFormInput
            rows={1}
            label='name'
            value={this.state.data.name}
            handleChange={this.handleChange}
            error={this.state.errors.name}
          />
          <MbFormInput
            rows={1}
            label='email'
            value={this.state.data.email}
            handleChange={this.handleChange}
            error={this.state.errors.email}
          />
          <MbFormInput
            rows={5}
            label='body'
            value={this.state.data.text}
            name='text'
            handleChange={this.handleChange}
            error={this.state.errors.text}
            multiline={true}
          />   
        </Grid>
      </form> 
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return state;
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
