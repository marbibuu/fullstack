import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Grid,Container} from '@material-ui/core';
import Comment from './Comment';
import {commentCtrl} from '../../logic/core'

//styles{{{
const styles=theme=>({
  toolbar:{
    background:'transparent',
    verticalAlign:'center',
  },
  title:{
    textTransform:'capitalize',
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'18',
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(){//{{{
    super();
    this.state={};
  }//}}}
  render(){//{{{
    const {classes,postID,comments,history}=this.props;
    return (
      <Container>
        <Grid container
          direction='column'
          spacing={3}
        >
        {(
          comments.map(comment=>{
            return (
              <Comment
                key={comment._id}
                comment={comment}
                postID={postID}
              />
            )
          })
        )}
        </Grid>
      </Container>
    );
  }//}}}
}//}}}

const mapStateToProps=(state,props)=>{//{{{
  return {
    postID:props.post._id,
    comments:commentCtrl.getComments(state,props.post.comments)
  }
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
