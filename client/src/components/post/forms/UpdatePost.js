import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Grid} from '@material-ui/core';

import MbFormInput from '../../utils/mbInput';

//styles{{{
const styles=theme=>({
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
      data:{
        title:props.post.title,
        text:props.post.text
      },
      errors:{
        title:'',
        text:''
      },
    }
  }//}}}
  handleChange=e=>{//{{{
    e.preventDefault();
    const {name,value}=e.target;
    let errors=this.state.errors;
    switch(name){
      case 'title':
        errors.title=value.length<=0?'Title should have at least one character.':'';
        break;
      case 'text':
        break;
      default:
        break;
    }
    this.setState({
      data:{...this.state.data,...{[name]:value}}
    })
    try{
      this.props.onChange(this.state.data);
    }catch(err){
    }
  }//}}}
  validateForm = (errors) => {//{{{
    let valid=true;
    Object.values(errors).forEach(
      (val) => val.length > 0 && (valid = false)
    );
    return valid;
  }//}}}
  handleSubmit=e=>{//{{{
    e.preventDefault();
    if(this.validateForm(this.state.errors)) {
      console.info('Valid Form')
      console.log(this.state)
    }else{
      console.error('Invalid Form')
    }
    return this.state.data;
  }//}}}
  render(){//{{{
    const {classes,post,history}=this.props;
    
    return (
      <form
        action='/'
        onSubmit={this.handleSubmit}
        autoComplete="off"
        > 
        <Grid container
          direction='column'
        >
          <MbFormInput
            rows={1}
            label='title'
            value={this.state.data.title}
            handleChange={this.handleChange}
            error={this.state.errors.title}
          />
          <MbFormInput
            rows={5}
            label='body'
            name='text'
            value={this.state.data.text}
            handleChange={this.handleChange}
            error={this.state.errors.text}
            multiline={true}
          />   
        </Grid>
      </form> 
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return state;
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
