import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {IconButton,Typography,ListItem,ListItemText,ListItemIcon,ListItemSecondaryAction} from '@material-ui/core'
import {DeleteOutlined,KeyboardArrowRight} from '@material-ui/icons';

import {postCtrl} from '../../logic/core';
import MbDialog from '../utils/Dialog';
import UpdatePostForm from '../post/forms/UpdatePost';

//styles{{{
const styles=theme=>({
  item:{
    border:'2px solid #000',
    marginBottom:'8px',
    userSelect:'none'
  },
  title:{
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'18',
  },
  removeBtn:{
    cursor:'pointer'
  }
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(props){//{{{
    super(props);
    this.state={
      dialog_updatePost_visible:false,
    };
  }//}}}
  //events{{{
  handleClick_select=e=>{//{{{
    console.log('select',this.post._id)
  }//}}}
  handleClick_remove=async e=>{//{{{
    console.log('remove',this.post._id)
    await postCtrl.delete({
      postID:this.post._id
    })
  }//}}}

  handleDblClick_showUpdatePostDialog=e=>{//{{{
    this.setUpdatePostDialogVisibility(true);
  }//}}}
  setUpdatePostDialogVisibility=value=>{//{{{
    this.setState({
      dialog_updatePost_visible:value
    })
  }//}}}
  handleClick_hideUpdatePostDialog=e=>{//{{{
    this.setUpdatePostDialogVisibility(false);
  }//}}}
  handleClick_updatePost=async (e,data)=>{//{{{
    await postCtrl.update({
      postID:this.props.post._id,
      ...data
    });
    this.setUpdatePostDialogVisibility(false)
  }//}}}

  //events}}}
  render(){//{{{
    const {classes,post,user,history}=this.props;
    this.post=post;
    const dialog=<MbDialog
          dialog={{
          timeout:3000,
          visible:this.state.dialog_updatePost_visible,
          title:"update post",
        }}
        onClose={this.handleClick_hideUpdatePostDialog}
        onAccept={this.handleClick_updatePost}
      >
        <UpdatePostForm
          post={this.props.post}
        />
      </MbDialog>
    return (
      <React.Fragment>
        <ListItem
          onDoubleClick={this.handleDblClick_showUpdatePostDialog}
          className={classes.item}
          elevation={5}
        >
          <ListItemIcon
            className={classes.removeBtn}
            onClick={this.handleClick_remove}
          >
            <DeleteOutlined
              color='primary'
            />
          </ListItemIcon>
          <ListItemText
            className={classes.title}
            primary={(
              <Typography
                noWrap={true}
              >{post.title}
              </Typography>
            )}
          />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              size='small'
              onClick={e=>{history.push(`/user/${user._id}/${post._id}`)}}
              >
              <KeyboardArrowRight
                fontSize='large'
                color='primary'
              />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      {dialog}
      </React.Fragment>
    );
  }//}}}
}//}}}
const mapStateToProps=(state,props)=>{//{{{
  return {
    post:props.post,
    user:props.user
  }
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
