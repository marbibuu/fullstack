import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {AppBar,Toolbar,Typography,Card,Button,Container} from '@material-ui/core';
import CommentList from '../comment/List';

import MbDialog from '../utils/Dialog';
import CreateCommentForm from '../comment/forms/CreateComment';

import {userCtrl,postCtrl,commentCtrl} from '../../logic/core';

//styles{{{
const styles=theme=>({
  toolbar:{
    background:'transparent',
    verticalAlign:'center',
  },
  title:{
    flexGrow: 1,
    textTransform:'capitalize',
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'18',
  },
  text:{
    fontSize:'15',
    textAlign:'left'
  },
});
//styles}}}
class Component extends React.Component{//{{{
  constructor(){//{{{
    super();
    this.state={
      commentsVisible:false,
      dialog_addComment_visible:false,
    };
  }//}}}
  handleClick_changeCommentVisibility=e=>{//{{{
    this.setState({
      commentsVisible:!this.state.commentsVisible
    });
    console.log('changeVisibility')
  }//}}}

  setAddCommentDialogVisibility=value=>{//{{{
    this.setState({
      dialog_addComment_visible:value
    })
  }//}}}
  handleClick_showAddCommentDialog=async e=>{//{{{
    this.setAddCommentDialogVisibility(true);
  }//}}}
  handleClick_hideAddCommentDialog=e=>{//{{{
    this.setAddCommentDialogVisibility(false);
  }//}}}
  handleClick_addComment=async (e,data)=>{//{{{
    await commentCtrl.create({
      postID:this.props.post._id,
      ...data
    });
    this.setAddCommentDialogVisibility(false)
  }//}}}

  render(){//{{{
    const {classes,user,post,history}=this.props;
    const dialog=<MbDialog
        dialog={{
          timeout:3000,
          visible:this.state.dialog_addComment_visible,
          title:"add comment",
        }}
        onClose={this.handleClick_hideAddCommentDialog}
        onAccept={this.handleClick_addComment}
      >
        <CreateCommentForm></CreateCommentForm>
      </MbDialog>
    return (
      <React.Fragment>
      <Container
      >
      <Card
        className='post'
        elevation={0}
      >
      <h3>{post.title}</h3>
      <p
        className={classes.text}
      >{post.text}</p>
      </Card>
      <AppBar
        className={classes.toolbar}
        elevation={0}
        position='static'
      >
      <Toolbar
        variant='dense'
      >

      <Button
        onClick={this.handleClick_changeCommentVisibility}
      >{this.state.commentsVisible?"hide comments":"show comments"}</Button>
      <Typography
      className={classes.title}
      >
      </Typography>
      {this.state.commentsVisible?(
        <Button
          onClick={this.handleClick_showAddCommentDialog}
        >Add comment
        </Button>
      ):""}
      </Toolbar>
      </AppBar>
      
      {this.state.commentsVisible?(
        <CommentList post={post}/>
      ):""}
      </Container>
      {dialog} 
      </React.Fragment>
    );
  }//}}}
}//}}}

const mapStateToProps=(state,props)=>{//{{{
  return {
    user:userCtrl.getByID(state,props.match.params.id),
    post:postCtrl.getByID(state,props.match.params.postID)
  };
}//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
