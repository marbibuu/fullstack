import React from 'react';
import {connect} from 'react-redux';
import {withStyles} from '@material-ui/core/styles';
import {withRouter} from 'react-router-dom';

import {Container,List} from '@material-ui/core'

import TrimmedPost from './Trimmed';

import {userCtrl,postCtrl} from '../../logic/core';

//styles{{{
const styles=theme=>({
  toolbar:{
    background:'transparent',
  },
  title:{
    textTransform:'capitalize',
    color:'#000',
    cursor:'default',
    fontWeight:'bold',
    fontSize:'18',
  } 
});
//styles}}}
class Component extends React.Component{//{{{
  render(){//{{{
    const {classes,posts,user,history}=this.props;
    return (
      <Container
        m={5}
      >
      <List>
      {posts.map(post=>{
        return (
          <TrimmedPost
            post={post}
            user={user}
            key={post._id}
          />)
      })}
      </List>
      </Container>
    );
  }//}}}
}//}}}
  const mapStateToProps=(state,props)=>{//{{{
    let user=userCtrl.getByID(state,props.match.params.id);
    return {
      user:user,
      posts:postCtrl.getPosts(state,user.posts)
    }
  }//}}}
export default connect(mapStateToProps)(withRouter(withStyles(styles)(Component)));
