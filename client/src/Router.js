import React from 'react';
import {connect} from 'react-redux';

import {BrowserRouter,Route,Switch} from 'react-router-dom';//Link
import {Card} from '@material-ui/core'

import UserDetail from './components/user/Detail';
import UserList from './components/user/List';
import PostDetail from './components/post/Detail';
import PostList from './components/post/List';
import Dialog from './components/utils/Dialog';

function Component() {
  return (
    <BrowserRouter>
      <div className="App">
        <Route exact path='/' component={UserList} />
        <Route path='/user/:id' component={UserDetail} />
        <Switch>
          <Route path='/user/:id/:postID' component={PostDetail} />
          <Route path='/user/:id' component={PostList} />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default Component;
