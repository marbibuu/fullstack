import React from 'react';
import {connect} from 'react-redux';

import Router from './Router';

var ctrl=require('./logic/user');

class App extends React.Component{
  constructor(props){
    super(props);  
    ctrl.getOnConnectData();
  }
  render(){
    return (
      <React.Fragment>
        <Router />
      </React.Fragment>
    );
  }
}
export default App;
