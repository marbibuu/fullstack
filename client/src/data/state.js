export default {//{{{
  entities:{
  users:{
    u1:{
    _id:'u1',
    name:'marco',
    surname:'polo',
    email:'marco.polo@gmail.com',
    mobile:'111-11-11-11',
    website:'www.marco.polo.com',
    architectures:[''],
    posts:['p1','p2']
  },
    u2:{
    _id:'u2',
    name:'amerigo',
    surname:'vespucci',
    email:'amerigo.vespucci@gmail.com',
    mobile:'222-11-11-11',
    website:'www.amerigo.vespucci.com',
    architectures:[''],
    posts:[]
  },
    u3:{
    _id:'u3',
    name:'kate',
    posts:[]
  },
    u4:{
    _id:'u4',
    name:'matt',
    posts:[]
  },
    u5:{
    _id:'u5',
    name:'brian',
    posts:[]
  }},
  posts:{
    p1:{
    _id:'p1',
    title:"pierwszy post sdf ksdf sdf ksd fks ddla dldf dfsl gsl fkdf glsad ks dgflwaef ked fgle;dnf fsjd frldg fksansfed fgrlesf d",
    text:'treść postu raz dwa trzy',
    comments:['c1','c2']
  },
    p2:{
    _id:'p2',
    title:"drugi",
    text:'cztery pieć sześć',
    comments:[]
  },
    p3:{
    _id:'p3',
    title:"trzeci",
    text:'siedem osiem dziewięć',
    comments:[]
  }},
  comments:{
    c1:{
    _id:'c1',
    name:'komentarz 1',
    email:'brian@gmail.com',
    text:'jakiś komentarz dla komentarza 1'
  },
    c2:{
    _id:'c2',
    name:'komentarz 2',
    email:'matt@gmail.com',
    text:'jakiś komentarz dla komentarza 2'
  }}
  }
};//}}}
