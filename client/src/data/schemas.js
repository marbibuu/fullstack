import {normalize,schema} from 'normalizr';
import config from '../config.json';
let opts={
  idAttribute:config.data.idAttribute
}

export const Comment=new schema.Entity('comments',{},opts);

export const Post=new schema.Entity('posts',{
  comments:[Comment]
},opts);

export const User=new schema.Entity('users',{
  posts:[Post]
},opts);
