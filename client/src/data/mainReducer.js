import initState from './state';

const mainReducer=(state=initState,action)=>{
  const newState={...state};
  let array;
  switch(action.type){
    //mozna to ew. rozbic na wiecej plikow ()
    //zamiast switcha mozna dac obiekt...
    //wtedy bedzie latwiej to rozbic na np. moduly
    case "SET_DATA_ON_CONNECT":
      newState.entities={...{
        users:{},
        posts:{},
        comments:{}
      },...action.data.entities}
      return newState;
    case "POST_CREATE":
      newState.entities.posts[action.data.post._id]=action.data.post;
      newState.entities.users[action.data.post.userID].posts.push(action.data.post._id);
      return newState;
    case "POST_UPDATE":
      newState.entities.posts[action.data.post._id]=action.data.post;
      return newState;
    case "POST_DELETE":
      array=newState.entities.users[action.data.userID].posts;
      array.splice(array.indexOf(action.data.postID),1);
      delete newState.entities.posts[action.data.postID];
      return newState;
    case "COMMENT_CREATE":
      newState.entities.comments[action.data.comment._id]=action.data.comment;
      newState.entities.posts[action.data.comment.postID].comments.push(action.data.comment._id);
      return newState;
    case "COMMENT_UPDATE":
      newState.entities.comments[action.data.comment._id]=action.data.comment;
      return newState;
    case "COMMENT_DELETE":
      array=newState.entities.posts[action.data.userID].posts;
      array.splice(array.indexOf(action.data.postID),1);
      delete newState.entities.comments[action.data.commentID];
      return newState;
    default:
      return state;
  }
}
export default mainReducer;
