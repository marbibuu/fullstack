var axios=require('axios'),
    CONFIG=require('../config.json');
class API{//{{{
  constructor(){//{{{
    this.url=`${CONFIG.URL}:${CONFIG.PORT}`;
  }//}}}
  async post(flag,data){//{{{
    return await axios.post(`${this.url}${flag}`,JSON.stringify(data));
  }//}}}
}//}}}
module.exports=new API();
