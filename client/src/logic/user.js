var Ctrl=require('./Ctrl'),
    {normalize}=require('normalizr');

class UserCtrl extends Ctrl{//{{
  constructor(){//{{{
    super("user");
  }//}}}
  async getOnConnectData(state,data){//{{{
    let res=await this.API.post('/api/onConnect',{});
    if(res.status===200){
      this.store.dispatch({
        type:'SET_DATA_ON_CONNECT',
        data:normalize(res.data.data,[this.schema.User])
      })
    }else{
      //moglby wyswietlic jakiegos snacka
    }
    //zwraca dane zeby np zakonczyc loader
    return res;
  }//}}}

  
}//}}}
module.exports=new UserCtrl();
