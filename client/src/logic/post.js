var Ctrl=require('./Ctrl'),
    {normalize}=require('normalizr');
class PostCtrl extends Ctrl{//{{{
  constructor(){//{{{
    super('post');
  }//}}}
  getPosts(state,_ids){//{{{
    return this.getItemsByIDs(state,_ids);
  }//}}}

}//}}}
module.exports=new PostCtrl();
