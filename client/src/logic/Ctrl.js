class Ctrl{//{{{
  //ID='_id'
  constructor(name){//{{{
    this.name=name;
    this.upperName=name.toUpperCase();
    this.collection=`${name}s`;
    this.API=require('./api');
    this.store=require('../data/store').default;
    this.schema=require('../data/schemas');
  }//}}}
  __compareIDs(_id1,_id2){//{{{
    return JSON.stringify(_id1)===JSON.stringify(_id2);
  }//}}}
  __getCollection(state){//{{{
    return state.entities[this.collection];
  }//}}}
  getByID(state,_id){//{{{
    return this.__getCollection(state)[_id];

    /*return this.__getCollection(state).find(item=>{
      return this.__compareIDs(_id,item._id);
    })*/
  }//}}}
  getItemsByIDs(state,_ids){//{{{
    return _ids.map(_id=>{
      return this.getByID(state,_id);
    })
  }//}}}
  getAll(state){//{{{
    return Object.values(this.__getCollection(state));
  }//}}}
  __notFoundError(){//{{{

  }//}}}
  __forbiddenError(){//{{{
    console.log('not implemented')
  }//}}}
  __handleError(res){
    switch(res.status){
      case 404:
        console.log('404 not implemented');
        break;
      case 403:
        console.log('403 not implemented');
        break;
      case 400:
        console.log('400 not implemented');
        break;
      case 500:
        console.log('500 not implemented');
        break;
      case 502:
        console.log('502 not implemented');
        break;
      case 503:
        console.log('503 not implemented');
        break;
    }
  }

  async __crud(operationName,data){//{{{
    try{
      let res=await this.API.post(`/api/${this.name}/${operationName}`,data);
      if(res.status===200){
        this.store.dispatch({
          type:`${this.upperName}_${operationName.toUpperCase()}`,
          data:res.data.data
        })
      }else{
        this.__handleError(res);
      }
      return res.data;
    }catch(err){
      console.log(err);
    }
  }//}}}
  async create(data){//{{{
    return await this.__crud("create",data);
  }//}}}
  async read(data){//{{{
    return await this.__crud("read",data);
  }//}}}
  async update(data){//{{{
    return await this.__crud("update",data);
  }//}}}
  async delete(data){//{{{
    return await this.__crud("delete",data);
  }//}}}
}//}}}
module.exports=Ctrl;
