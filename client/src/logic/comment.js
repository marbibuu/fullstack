var Ctrl=require('./Ctrl'),
    {normalize}=require('normalizr');
class CommentCtrl extends Ctrl{//{{{
  constructor(){//{{{
    super("comment");
  }//}}}
  getComments(state,_ids){//{{{
    return this.getItemsByIDs(state,_ids);
  }//}}}

}//}}}
module.exports=new CommentCtrl();
