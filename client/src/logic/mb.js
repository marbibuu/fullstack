class mb{//{{{
  async sleep(timeout,e,cb,data){//{{{
    return new Promise((resolve,reject)=>{
      setTimeout(async ()=>{
        resolve(await cb(e,data));
      },timeout)
    })
  }//}}}
  validateEmail(value){//{{{
    let re =/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i; 
    return re.test(value);
  }//}}}
}//}}}
module.exports=new mb();
