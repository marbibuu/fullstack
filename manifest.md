(To read this in normal way choose Raw on Toolbar :)

Fullstack

based on
https://github.com/blueappsgroup/recruitment

getting repo
git clone https://bitbucket.org/marbibuu/fullstack/src/master/

preparing backend
cd server
npm install
npm run mongo  //for now only mongo db is plugged soon will plug also postgres
npm run server //after mongo is ready

preparing frontend
cd client
npm install
npm run dev

*Server is used only as API provider
