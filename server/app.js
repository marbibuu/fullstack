const http = require('http'),
      PORT=process.env.PORT || 3001;
console.clear();

class API{//{{{
  constructor(){//{{{
    //mozna wrzucic do jakiegos pliku konfiguracyjnego
    //ogolnie wszystkie stale powinny tam wyladowac...
    this.cmdOpts={
      db:'mongo',//mongo//postgres
      timeout:3000,
    };
    this.__runDesiredDB().connect();
    this.cmds=require('./api/api');
  }//}}}
  __runDesiredDB(){//{{{
    return require(`./db/${this.cmdOpts.db}/db`);
  }//}}}
  setHeaders(res){//{{{
    res.setHeader('Content-type','application/json');
    res.setHeader('Access-Control-Allow-Origin','*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS,GET,POST');
    res.setHeader('Access-Control-Allow-Headers', '*');
    res.setHeader('Access-Control-Allow-Credentials', true);
  }//}}}
  post(req,res){//{{{
    var body = '',
        result;
    req.on('data',(chunk)=>{
      body += chunk;
      if (body.length > 1e6)
        req.connection.destroy();
    });
    req.on('end',async ()=>{
      try{
        result=await this.run(req.url,JSON.parse(body));
      }catch(err){
        result={}
        console.log(err)
      }
      res.end(JSON.stringify(result));
    })
  }//}}}
  async run(url,data){//{{{
    console.log(`--> client: ${url} \ndata:\n${JSON.stringify(data,undefined,2)}`)
    let result=await this.cmds[url].run(data,this.cmdOpts);
    console.log(`<-- ${JSON.stringify(result,undefined,2)}`)
    return result
  }//}}}
}//}}}
var api=new API();
const server=http.createServer((req,res)=>{
  api.setHeaders(res);
  res.writeHead(200);
  switch(req.method){
    case 'POST':
      api.post(req,res);
      break;
    case 'GET':
      console.log('GET is not yet implemented')
      break;
    default:
      res.end('API');
  }
})

server.listen(PORT,(err)=>{
  if(err){
    console.log('error')  
  }else{
    console.log(`Server is running on ${PORT}`)
  }
})

