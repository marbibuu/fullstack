module.exports={
  '/':async data=>{},
  '/api/onConnect':require('./user/cmds/onConnect'),
  '/api/post/create':require('./post/cmds/create'),

  '/api/post/update':require('./post/cmds/update'),
  '/api/post/delete':require('./post/cmds/delete'),

  '/api/comment/create':require('./comment/cmds/create'),
  '/api/comment/update':require('./comment/cmds/update'),
  '/api/comment/delete':require('./comment/cmds/delete'),
  /*
  '/api/post/read':require('./post/cmds/read'),

  '/api/comment/read':require('./comment/cmds/read'),
  */
}
