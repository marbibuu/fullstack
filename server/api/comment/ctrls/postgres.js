const {db}=require('../../../db/postgres/schemas');
class Ctrl{//{{{
  async create(data,opts){//{{{
    let comment=(await db.query({
      text:`INSERT INTO comments(name,text,email,"postID") VALUES($1,$2,$3,$4) RETURNING *`,
      values:[data.name,data.text,data.email,data.postID]
    })).rows[0];
    return {
      comment:comment
    }
  }//}}}
  async delete(data,opts){//{{{
    /*let post=(await db.query({
      text:`DELETE FROM posts WHERE _id=$1 RETURNING *`,
      values:[data.postID]
    })).rows[0];
    console.log(post)
    
    return {
      userID:post.userID,
      postID:data.postID
    }*/
  }//}}}
  async update(data,opts){//{{{
    let comment=(await db.query({
      text:`UPDATE comments SET name=$2,text=$3,email=$4 WHERE _id=$1 RETURNING *`,
      values:[data.commentID,data.name,data.text,data.email]
    })).rows[0]
    return {
      comment:comment
    }
  }//}}}
}//}}} 
module.exports=new Ctrl();
