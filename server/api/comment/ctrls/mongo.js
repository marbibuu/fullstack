const db=require('../../../db/mongo/schemas');
class Ctrl{//{{{
  async create(data,opts){//{{{
    let post=await db.post.findOne({
          _id:data.postID
        }).exec(),
        comment=await db.comment.create({
          postID:post._id,
          name:data.name,
          text:data.text,
          email:data.email
        });
    post.comments.push(comment._id);
    await post.save();
    return {
      comment:comment
    }
  }//}}}
  async delete(data,opts){//{{{
    let comment=await db.comment.findOne({
      _id:data.commentID
    }).populate('postID').exec();

    comment.postID.comments.splice(comment.postID.comments.indexOf(data.commentID),1);
    await comment.postID.save();
    await comment.remove();
    return {
      postID:comment.postID._id,
      commentID:data.commentID
    }
  }//}}}
  async update(data,opts){//{{{
    let comment=await db.comment.findOne({
      _id:data.commentID
    }).exec();
    
    comment.name=data.name;
    comment.text=data.text;
    comment.email=data.email;

    return {
      comment:await comment.save()
    }
  }//}}}
}//}}} 
module.exports=new Ctrl();
