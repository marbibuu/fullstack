var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('update',{
      onSuccess:`Comment was successfully updated.`,
      onFail:`Comment cannot be updated.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.update(data,opts);
    }catch(err){
      console.log(err)
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
