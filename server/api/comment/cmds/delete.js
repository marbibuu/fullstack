var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('delete',{
      onSuccess:`Comment was successfully removed.`,
      onFail:`Comment cannot be removed.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.delete(data,opts);
    }catch(err){
      console.log(err)
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
