var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('create',{
      onSuccess:`Comment was successfully created.`,
      onFail:`Comment cannot be created.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.create(data,opts);
    }catch(err){
      console.log(err)
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
