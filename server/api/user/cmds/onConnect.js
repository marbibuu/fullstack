var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('onConnect',{
      onSuccess:`Client received default data on connect.`,
      onFail:`Client cannot receive default data on connect.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.getPopulatedUsers(data,opts);
    }catch(err){
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
