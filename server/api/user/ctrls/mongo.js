const db=require('../../../db/mongo/schemas');
class Ctrl{//{{{
  async getPopulatedUsers(){//{{{
    return await db.user.find().populate({
      path:'posts',
      populate:{
        path:'comments'
      }
    }).exec();
  }//}}}
}//}}} 
module.exports=new Ctrl();
