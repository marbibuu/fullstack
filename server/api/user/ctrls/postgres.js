const {db}=require('../../../db/postgres/schemas');
class Ctrl{//{{{
  async getPopulatedUsers(){//{{{
    return (await db.query(`
      SELECT row_to_json(t) FROM (
        SELECT *,(
          SELECT coalesce(json_agg(row_to_json(p)),'[]'::json) FROM (
            SELECT *,(
                SELECT coalesce(json_agg(row_to_json(comments)),'[]'::json) FROM comments WHERE "postID"=posts._id
              ) AS comments FROM posts
            )p 
          WHERE "userID"=users._id
        ) AS posts FROM users
      )t`)).rows.map(item=>{
        return item['row_to_json'];
      })
  }//}}}
}//}}} 
module.exports=new Ctrl();
