class Command{//{{{
  constructor(name,comments){//{{{
    this.name=name;
    this.comments=comments;
  }//}}}
  async run(data,opts){//{{{
    /*mozna zaimplementowac walidatory, ktore zanim uruchomia
      komende, posprawdzaja dane i ew. zwroca blad do klienta*/
    try{
      return {
        status:true,
        comment:this.comments.onSuccess,
        data:await this._run(data,opts)
      }
    }catch(err){
      console.log(err)
      return {
        status:false,
        comment:this.comments.onFail,
        data:{}
      }
    }
  }//}}}
  async _run(data,opts){//{{{
    console.log(`Command:${this.name} not yet implemented.`)
    return new Promise((resolve)=>{
      resolve({});
    });
  }//}}}
}//}}}
module.exports=Command;
