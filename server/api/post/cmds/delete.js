var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('delete',{
      onSuccess:`Post was successfully removed.`,
      onFail:`Post cannot be removed.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.delete(data,opts);
    }catch(err){
      console.log(err)
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
