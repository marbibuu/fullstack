var Cmd=require('../../Command');
class Command extends Cmd{//{{{
  constructor(){//{{{
    super('update',{
      onSuccess:`Post was successfully updated.`,
      onFail:`Post cannot be updated.`
    })
  }//}}}
  async _run(data,opts){//{{{
    let ctrl=require(`../ctrls/${opts.db}`);
    try{
      return await ctrl.update(data,opts);
    }catch(err){
      console.log(err)
      return {};
    }
  }//}}}
}//}}}
module.exports=new Command();
