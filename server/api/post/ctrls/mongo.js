const db=require('../../../db/mongo/schemas');
class Ctrl{//{{{
  async create(data,opts){//{{{
    let user=await db.user.findOne({
          _id:data.userID
        }).exec(),
        post=await db.post.create({
          userID:user._id,
          title:data.title,
          text:data.text
        });
    user.posts.push(post._id);
    await user.save();
    return {
      post:post
    }
  }//}}}
  async delete(data,opts){//{{{
    let post=await db.post.findOne({
      _id:data.postID
    }).populate('userID').exec();

    post.userID.posts.splice(post.userID.posts.indexOf(data.postID),1);
    await post.userID.save();
    await post.remove();
    for(let i=0;i<post.comments.length;i++){
      await db.comment.findOne({_id:post.comments[i]}).deleteOne();
    }
    return {
      userID:post.userID._id,
      postID:data.postID
    }
  }//}}}
  async update(data,opts){//{{{
    let post=await db.post.findOne({
      _id:data.postID
    }).exec();
    post.title=data.title;
    post.text=data.text;
    return {
      post:await post.save()
    }
  }//}}}
}//}}} 
module.exports=new Ctrl();
