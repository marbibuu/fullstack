const {db}=require('../../../db/postgres/schemas');
class Ctrl{//{{{
  async create(data,opts){//{{{
    let post=(await db.query({
      text:`INSERT INTO posts(title,text,"userID") VALUES($1,$2,$3) RETURNING *`,
      values:[data.title,data.text,data.userID]
    })).rows[0];
    post.comments=[];
    return {
      post:post
    }
  }//}}}
  async delete(data,opts){//{{{
    let post=(await db.query({
      text:`DELETE FROM posts WHERE _id=$1 RETURNING *`,
      values:[data.postID]
    })).rows[0];
    
    return {
      userID:post.userID,
      postID:data.postID
    }
  }//}}}
  async update(data,opts){//{{{
    let post=(await db.query({
      text:`UPDATE posts SET title=$2,text=$3 WHERE _id=$1 RETURNING *`,
      values:[data.postID,data.title,data.text]
    })).rows[0]
    return {
      post:post
    }
  }//}}}
}//}}} 
module.exports=new Ctrl();
