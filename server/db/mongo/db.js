class Db{//{{{
  constructor(){//{{{
    this.schema;
    this.name='Mongo';
    this.db=require('mongoose');
    this.db.Promise=Promise;
    this.replicas='mongodb://localhost:27017,localhost:27018,localhost:27019/txn';
    this.CONNECTION_URI=process.env.MONGODB_URI || this.replicas;
  }//}}}
  __prepareOptions(){//{{{
    if(this.CONNECTION_URI===this.replicas){
      console.log('Transactions will be available.')
      return {
        useUnifiedTopology: true,
        keepAlive:1,
        family:4,
        useNewUrlParser:true,
        replicaSet:'rs'
      }
    }else{
      console.log("Transactions won't be availabale.")
    }
    return {
      useNewUrlParser:true,
    }
  }//}}}
  async __connect(){//{{{
    let opts=this.__prepareOptions();
    await this.db.connect(this.CONNECTION_URI,opts)
  }//}}}
  async connect(){//{{{
    try{
      await this.__connect();
      console.log('Mongo db is connected.')
      this.schema=this.createSchemas();
      await this.clear();
      await this.prepareDefaultData();
      console.log('Waiting for client...')
    }catch(err){
      console.log(err)
    }
  }//}}}
  createSchemas(){//{{{
    return require('./schemas');
  }//}}}
  async clear(){//{{{
    let schemas=Object.keys(this.schema);
    for(let i=0;i<schemas.length;i++){
      await this.schema[schemas[i]].deleteMany().exec();
    }
  }//}}}
  async prepareDefaultData(schemas){//{{{
    //wrzucic sesje
    let u1=await this.schema.user({
      name:'marco',
      surname:'polo',
      email:'marco@gmail.com',
      website:'www.marco.com',
      mobile:'111-11-11-11',
      /*posts:[
        await this.schema.post({
          title:'tytul postu',
          text:'tresc postu',
          comments:[
            await this.schema.comment({
              name:'kommentarz 1',
              text:'treść komentarza 1'
            }).save(),
            await this.schema.comment({
              name:'kommentarz 2',
              text:'treść komentarza 2'
            }).save(),
          ]
        }).save(),
        await this.schema.post({
          title:'tytul postu2',
          text:'tresc postu2',
          comments:[
            await this.schema.comment({
              name:'kommentarz 3',
              text:'treść komentarza 3'
            }).save(),
          ]
        }).save()
      ]*/
    }).save();
    


    await this.schema.user({
      name:'kate',
      surname:'bush',
      email:'kate@gmail.com',
      website:'www.kate.com',
      mobile:'222-22-22-22',
      posts:[]
    }).save(); 


  }//}}}
  async getAllUsers(){//{{{
    return await this.schema.user.find().exec();
  }//}}}
}//}}}

module.exports=new Db();
