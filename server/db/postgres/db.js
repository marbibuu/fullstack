class Db{//{{{
  constructor(){//{{{
    this.name='Postgres';
    this.db;
    this.schema;
    this.CONNECTION_URI="localhost";//process.env.MONGODB_URI || this.replicas;
  }//}}}
  __getURI(){//{{{
    return 'localhost'
  }//}}}
  __prepareOptions(){//{{{
    return {
    
    }
  }//}}}
  async __connect(){//{{{
    let opts=this.__prepareOptions();//co z tym?
    this.schema=await this.createSchemas();
  }//}}}
  async connect(){//{{{
    try{
      await this.__connect();
      console.log(`${this.name} db is connected`);
      await this.clear();
      await this.prepareDefaultData();
      console.log('Waiting for client...');
    }catch(err){
      console.log(err)
    }
  }//}}}
  async clear(){//{{{
    return await this.db.query(`TRUNCATE ${this.schema.tables};`)
  }//}}}
  async createSchemas(){//{{{
    let schemas=require('./schemas');
    this.db=schemas.db;
    await this.db.query(schemas.schemas);
    return schemas;
  }//}}}
  async prepareDefaultData(){//{{{
    let query;
    query = await this.db.query({
      text:'INSERT INTO users(name,surname,email,mobile,website) VALUES($1,$2,$3,$4,$5) RETURNING *',
      values:['marco','polo','marco@gmail.com','111-11-11-11','www.marco.com']
    })
    query = await this.db.query({
      text:'INSERT INTO users(name,surname,email,mobile,website) VALUES($1,$2,$3,$4,$5) RETURNING *',
      values:['kate','bush','kate@gmail.com','222-22-22-22','www.kate.com']
    })
    query = await this.db.query({
      text:'INSERT INTO posts(title,text,"userID") VALUES($1,$2,$3) RETURNING *',
      values:['some title1','some text1',query.rows[0]._id]
    })
    query = await this.db.query({
      text:'INSERT INTO comments(name,text,email,"postID") VALUES($1,$2,$3,$4) RETURNING *',
      values:['some comment1 name','some comment1 text','a@gmail.com',query.rows[0]._id]
    })
    query=await this.db.query('SELECT * FROM users')
    //console.log(query.rows)
    query=await this.db.query('SELECT * FROM posts')
    //console.log(query.rows)
    query=await this.db.query('SELECT * FROM comments')
    //console.log(query.rows)
    
    query=await this.db.query(`
        SELECT row_to_json(t) FROM (
          SELECT * FROM users
        )t
      `)
    query=(await this.db.query(`
      SELECT row_to_json(t) FROM (
        SELECT *,(
          SELECT coalesce(json_agg(row_to_json(p)),'[]'::json) FROM (
            SELECT *,(
                SELECT coalesce(json_agg(row_to_json(comments)),'[]'::json) FROM comments WHERE "postID"=posts._id
              ) AS comments FROM posts
            )p 
          WHERE "userID"=users._id
        ) AS posts FROM users
      )t`)).rows.map(item=>{
      return item['row_to_json']
    })
    query=await this.db.query(`
      SELECT row_to_json(t) FROM (
        SELECT title,text,(
          SELECT json_agg(row_to_json(c)) FROM (SELECT name,text,"postID" FROM comments)c WHERE "postID"=posts._id
        ) AS comments FROM posts
      )t`)
  }//}}}
}//}}}
module.exports=new Db();
