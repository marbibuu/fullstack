const {Pool}=require('pg');
module.exports={
  tables:"comments,posts,users",
  schemas:`
      DROP TABLE users,posts,comments;
      CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
      CREATE TABLE IF NOT EXISTS users(
        _id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1(),
        name VARCHAR(40) not null,
        surname VARCHAR(40) not null,
        email VARCHAR(40) not null,
        mobile VARCHAR(40) not null,
        website VARCHAR(40) not null);
      CREATE TABLE IF NOT EXISTS posts(
        _id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1(),
        title VARCHAR(40) not null,
        text VARCHAR(1024) not null,
        "userID" UUID REFERENCES users(_id));
      CREATE TABLE IF NOT EXISTS comments(
        _id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v1(),
        name VARCHAR(40) not null,
        text VARCHAR(1024) not null,
        email VARCHAR(1024) not null,
        "postID" UUID REFERENCES posts(_id) ON DELETE CASCADE);`,
  db:new Pool({
    host:'localhost',
    user:'marek',
    password:'',
    database:'marek'
  })
}
